import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Header from './header/header.component.jsx';
import MessageList from './message-list/message-list.component.jsx';
import MessageInput from './message-input/message-input.component.jsx';
import MessageEdit from './message-list/message-edit/message-edit.component.jsx';
import { fetchMessages, editLastMessage } from './message-list/actions';

class Chat extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchMessages());
    document.addEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown = (event) => {
    const { isLoading, messageToEdit, dispatch } = this.props;
    if (!isLoading && !messageToEdit) {
      if (event.keyCode === 38) {
        dispatch(editLastMessage());
      }
    }
  };

  render() {
    const { isLoading, messageToEdit } = this.props;

    return (
      <div className="chat">
        <Header />
        <MessageList />{' '}
        {isLoading && <div className="loader"> Loading... </div>}{' '}
        {!isLoading && <MessageInput addMessage={this.addMessage} />}{' '}
        {messageToEdit && (
          <MessageEdit
            message={messageToEdit}
            cancel={this.cancelEdit}
            update={this.updateMessage}
          />
        )}{' '}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.messages.isLoading,
    messageToEdit: state.messages.messageToEdit
  };
};

Chat.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  messageToEdit: PropTypes.shape({
    id: PropTypes.string.isRequired,
    likesCount: PropTypes.number.isRequired,
    userId: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    createdAt: PropTypes.string,
    editedAt: PropTypes.string
  })
};

export default connect(mapStateToProps)(Chat);
