import {
  createStore,
  combineReducers,
  applyMiddleware
} from 'redux';
import {
  composeWithDevTools
} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import messagesReducer from './message-list/reducer';

const reducers = {
  messages: messagesReducer
};

const rootReducer = combineReducers({
  ...reducers
});

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default store;