import {
  ADD_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  TOGGLE_LIKE,
  REQUEST_MESSAGES,
  RECEIVE_MESSAGES,
  UPDATE_MESSAGE,
  CANCEL_EDIT_MESSAGE,
  EDIT_LAST_MESSAGE
} from './actionTypes';

export const addMessage = (text) => ({
  type: ADD_MESSAGE,
  payload: text
});

export const editMessage = (id) => ({
  type: EDIT_MESSAGE,
  payload: id
});

export const editLastMessage = () => ({
  type: EDIT_LAST_MESSAGE
});

export const cancelEditMessage = () => ({
  type: CANCEL_EDIT_MESSAGE
});

export const updateMessage = (id, text) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    text
  }
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: id
});

export const toggleLike = (id) => ({
  type: TOGGLE_LIKE,
  payload: id
});

export const requestMessages = () => {
  return {
    type: REQUEST_MESSAGES
  };
};

export const receiveMessages = (json) => {
  const messages = json.map((message) => {
    return {
      likesCount: 0,
      ...message
    };
  });
  return {
    type: RECEIVE_MESSAGES,
    payload: messages
  };
};

export const fetchMessages = () => {
  return async (dispatch) => {
    dispatch(requestMessages());
    const response = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json');
    const messages = await response.json();
    dispatch(receiveMessages(messages));
  };
};