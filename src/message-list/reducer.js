import {
  ADD_MESSAGE,
  EDIT_MESSAGE,
  EDIT_LAST_MESSAGE,
  CANCEL_EDIT_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  RECEIVE_MESSAGES,
  REQUEST_MESSAGES,
  TOGGLE_LIKE
} from './actionTypes';
import moment from 'moment';
import {
  v4 as uuidv4
} from 'uuid';

const initialState = {
  chatName: 'myChat',
  isLoading: true,
  messageToEdit: null,
  scrollDown: true,
  messages: [],
  likes: [],
  currentUser: {
    id: 'myId',
    name: 'Andrii',
    avatar: `https://robohash.org/myId?set=set2`,
  }
};

export const messagesReducer = (state = initialState, action) => {
  const {
    type,
    payload
  } = action;
  switch (type) {
    case REQUEST_MESSAGES:
      return {
        ...state,
        isLoading: true
      };
    case RECEIVE_MESSAGES: {
      const messageList = [...payload];
      // Sorting messages by date descending
      messageList.sort((a, b) => new Date(b).getTime() - new Date(a).getTime());
      return {
        ...state,
        isLoading: false,
        messages: messageList,
        error: ''
      };
    }

    case ADD_MESSAGE: {
      const {
        currentUser,
        messages
      } = state;
      const message = {
        id: uuidv4(),
        userId: currentUser.id,
        avatar: currentUser.avatar,
        user: currentUser.name,
        text: payload,
        editedAt: '',
        createdAt: moment(Date.now()).format(),
        likesCount: 0
      };
      return {
        ...state,
        messages: [...messages, message],
        scrollDown: true
      };
    }

    case EDIT_MESSAGE: {
      const {
        messages
      } = state;
      const messageToEdit = messages.find((message) => message.id === payload);

      return {
        ...state,
        messageToEdit: messageToEdit,
        scrollDown: false,
      };
    }

    case EDIT_LAST_MESSAGE: {
      const {
        currentUser,
        messages
      } = state;

      const curentUserMessages = messages.filter(message => message.userId === currentUser.id);
      let lastMessage = [...curentUserMessages]
        .map((message) => {
          return {
            id: message.id,
            date: new Date(message.createdAt).getTime()
          }
        })
        .sort((a, b) => b.date - a.date)[0];
      if (lastMessage) {
        const messageToEdit = messages.find((message) => message.id === lastMessage.id);
        return {
          ...state,
          messageToEdit: messageToEdit,
          scrollDown: false,
        };
      } else {
        return state;
      }
    }

    case CANCEL_EDIT_MESSAGE:
      return {
        ...state,
        messageToEdit: null,
          scrollDown: true
      }

      case UPDATE_MESSAGE: {
        const {
          messages
        } = state;
        const {
          id,
          text
        } = payload;
        const messageList = [...messages];
        const messageToUpdate = messageList.find(
          (message) => message.id === id
        );
        if (messageToUpdate) {
          messageToUpdate.text = text;
        }
        return {
          ...state,
          messages: messageList,
          messageToEdit: null,
          scrollDown: false,
        }
      }

      case DELETE_MESSAGE: {
        const {
          messages
        } = state;
        const messageList = [...messages];

        const index = messageList.findIndex((message) => message.id === payload);
        if (index !== -1) {
          messageList.splice(index, 1);
        }
        return {
          ...state,
          messages: messageList,
          scrollDown: false,
        }
      }

      case TOGGLE_LIKE: {
        const {
          likes,
          currentUser,
          messages
        } = state;
        const index = likes.findIndex(
          (like) => like.id === payload && like.userId === currentUser.Id
        );
        const messageList = [...messages];
        if (index !== -1) {
          likes.splice(index, 1);
          messageList.find((message) => message.id === payload).likesCount -= 1;
        } else {
          likes.push({
            id: payload,
            userId: currentUser.Id
          });
          messageList.find((message) => message.id === payload).likesCount += 1;
        }
        return {
          ...state,
          messages: messageList,
          scrollDown: false
        }
      }

      default:
        return state;
  }
};

export default messagesReducer;