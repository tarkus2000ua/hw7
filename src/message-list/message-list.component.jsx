import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';
import PropTypes from 'prop-types';
import MessageItem from './message-item/message-item.component';
import formatDate from '../helpers/formatdate';

import './message-list.styles.css';

const MessageList = ({
  messages,
  toggleLike,
  deleteMessage,
  editMessage,
  currentUser,
  scrollDown
}) => {
  // Scroll Down effect on initial load and for addMmessage action
  const messagesEndRef = useRef(null);
  const scrollToBottom = () => {
    if (messagesEndRef.current && scrollDown) {
      messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };
  useEffect(scrollToBottom, [messages]);
  let prevDate, currentDate;
  return (
    <div className="container">
      <div className="message-list">
        {messages &&
          messages.map((message) => {
            currentDate = new Date(message.createdAt).getDate();

            const messageItem = (
              <MessageItem
                key={message.id}
                message={message}
                toggleLike={toggleLike}
                deleteMessage={deleteMessage}
                editMessage={editMessage}
                currentUser={currentUser}
              />
            );

            if (currentDate !== prevDate) {
              prevDate = currentDate;
              // Add splitting line between different dates
              return (
                <div className="break-wrapper" key={message.createdAt}>
                  <div className="break">{formatDate(message.createdAt)}</div>
                  {messageItem}
                </div>
              );
            } else {
              prevDate = currentDate;
              return (
                <MessageItem
                  key={message.messageId}
                  message={message}
                  toggleLike={toggleLike}
                  deleteMessage={deleteMessage}
                  editMessage={editMessage}
                  currentUser={currentUser}
                />
              );
            }
          })}
        <div ref={messagesEndRef} />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    messages: state.messages.messages,
    currentUser: state.messages.currentUser,
    scrollDown: state.messages.scrollDown
  };
};

const mapDispatchToProps = {
  ...actions
};

MessageList.propTypes = {
  messages: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      likesCount: PropTypes.number.isRequired,
      userId: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      user: PropTypes.string.isRequired,
      avatar: PropTypes.string.isRequired,
      createdAt: PropTypes.string,
      editedAt: PropTypes.string
    })
  ).isRequired,
  scrollDown: PropTypes.bool.isRequired,
  currentUser: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
