import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '../actions';

import './message-edit.styles.css';

class MessageEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: this.props.message.text
    };
  }

  onChange = (e) => {
    this.setState({ text: e.target.value });
  };

  onSubmit = (e) => {
    const { message, updateMessage } = this.props;
    e.preventDefault();
    updateMessage(message.id, this.state.text);
    this.setState({ text: '' });
  };

  render() {
    const { cancelEditMessage } = this.props;
    return (
      <div className="message-edit-modal">
        <div className="header">Edit message</div>
        <form className="message-edit" onSubmit={(e) => this.onSubmit(e)}>
          <div className="edit-wrapper">
            <textarea
              className="text"
              value={this.state.text}
              onChange={(e) => this.onChange(e)}
            ></textarea>
            <button type="submit" className="btn btn-save">
              Save
            </button>
            <button className="btn btn-cancel" onClick={cancelEditMessage}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = {
  ...actions
};

MessageEdit.propTypes = {
  message: PropTypes.shape({
    id: PropTypes.string.isRequired,
    likesCount: PropTypes.number.isRequired,
    userId: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    createdAt: PropTypes.string,
    editedAt: PropTypes.string
  }).isRequired
};

export default connect(null, mapDispatchToProps)(MessageEdit);
